﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BE
{
    public class CONCEPTO
    {

        private int id;

        public int ID
        {
            get { return id; }
            set { id = value; }
        }

        private string descripcion;

        public string Descripcion
        {
            get { return descripcion; }
            set { descripcion = value; }
        }

        private float porcentaje;

        public float Porcentaje
        {
            get { return porcentaje; }
            set { porcentaje = value; }
        }

        public override string ToString()
        {
            return descripcion + " - " + porcentaje.ToString() + "%";
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BE
{
    public class SUELDO
    {

        private int id;

        public int ID
        {
            get { return id; }
            set { id = value; }
        }


        private float sueldoBruto;

        public float SueldoBruto
        {
            get { return sueldoBruto; }
            set { sueldoBruto = value; }
        }

        private float sueldoNeto;

        public float SueldoNeto
        {
            get { return sueldoNeto; }
            set { sueldoNeto = value; }
        }

        private float totalDescuentos;

        public float TotalDescuentos
        {
            get { return totalDescuentos; }
            set { totalDescuentos = value; }
        }

        public override string ToString()
        {
            return SueldoNeto.ToString();
        }
    }
}
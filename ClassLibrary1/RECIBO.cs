﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BE
{
    public class RECIBO
    {
        private int id;

        public int Id
        {
            get { return id; }
            set { id = value; }
        }


        private string fecha;

        public string Fecha
        {
            get { return fecha; }
            set { fecha = value; }
        }

        private EMPLEADO empleado;

        public EMPLEADO EMPLEADO
        {
            get { return empleado; }
            set { empleado = value; }
        }

        private List<CONCEPTO> conceptosaplicados = new List<CONCEPTO>();

        public List<CONCEPTO> ConceptosAplicados
        {
            get { return conceptosaplicados; }
            set { conceptosaplicados = value; }
        }

        private float descuentos;

        public float Descuentos
        {
            get { return descuentos; }
            set { descuentos = value; }
        }


        private float subtotal;

        public float Subtotal
        {
            get { return subtotal; }
            set { subtotal = value; }
        }

        private float total;

        public float Total
        {
            get { return total; }
            set { total = value; }
        }



    }
}
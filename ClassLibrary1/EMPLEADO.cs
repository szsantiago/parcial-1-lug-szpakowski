﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BE
{
    public class EMPLEADO
    {

        private int legajo;

        public int Legajo
        {
            get { return legajo; }
            set { legajo = value; }
        }

        private string nombre;

        public string Nombre
        {
            get { return nombre; }
            set { nombre = value; }
        }

        private string apellido;

        public string Apellido
        {
            get { return apellido; }
            set { apellido = value; }
        }

        private string cuil;

        public string CUIL
        {
            get { return cuil; }
            set { cuil = value; }
        }

        private DateTime alta;

        public DateTime Alta
        {
            get { return alta; }
            set { alta = value; }
        }

        private List<CONCEPTO> conceptos = new List<CONCEPTO>();

        public List<CONCEPTO> Conceptos
        {
            get { return conceptos; }
            set { conceptos = value; }
        }

        private SUELDO sueldo;

        public SUELDO SUELDO
        {
            get { return sueldo; }
            set { sueldo = value; }
        }

        public override string ToString()
        {
            return nombre + " " + apellido; 
        }
    }

}
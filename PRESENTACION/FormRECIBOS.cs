﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PRESENTACION
{
    public partial class FormRECIBOS : Form
    {
        BE.EMPLEADO empleado = new BE.EMPLEADO();
        BLL.RECIBO recibo = new BLL.RECIBO();

        public FormRECIBOS()
        {
            InitializeComponent();
        }

        private void FormRECIBOS_Load(object sender, EventArgs e)
        {
            dataGridView1.DataSource = null;
            dataGridView1.DataSource = BLL.EMPLEADO.Listar();
            button4_Click(null, e);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (empleado.Nombre != null || empleado.Legajo != 0)
            {
                List<BE.RECIBO> recibos = BLL.RECIBO.Listar();
                if (recibos.Count == 0)
                {
                    recibo.Alta(empleado, dateTimePicker1.Text);   // Primer recibo
                }
                else
                {
                    bool ok = true;
                                    


                    foreach (BE.RECIBO r in recibos)        
                    {
                        if (empleado.Legajo == r.EMPLEADO.Legajo && dateTimePicker1.Text == r.Fecha)
                        {
                            ok = false;             // EL RECIBO YA ESTA CARGADO
                        }
                    }

                    if (ok)
                    {
                        recibo.Alta(empleado, dateTimePicker1.Text);    //CARGAR NUEVO RECIBO
                    }
                    else
                    {
                        MessageBox.Show("El recibo para el empleado y fecha elegida ya se cargo en el sistema");
                    }

                    button4_Click(null, e);
                }
            }
            else
            {
                MessageBox.Show("Seleccionar empleado");
            }
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            empleado = (BE.EMPLEADO)dataGridView1.SelectedRows[0].DataBoundItem;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (dataGridView1.SelectedRows.Count != 0)
            {
                dataGridView2.DataSource = null;

                List<BE.RECIBO> recibos = BLL.RECIBO.Listar();

                BE.EMPLEADO empleado = (BE.EMPLEADO)dataGridView1.SelectedRows[0].DataBoundItem;

                List<BE.RECIBO> recibosEmpleado = (from r in recibos
                                                   where r.EMPLEADO.Legajo == empleado.Legajo
                                                   select r
                                                    ).ToList();

                dataGridView2.DataSource = recibosEmpleado;

                listBox1.DataSource = null;
            }
            else
            {
                MessageBox.Show("Seleccionar empleado");
            }
        }

        private void dataGridView2_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            listBox1.Items.Clear();

            BE.RECIBO r = (BE.RECIBO)dataGridView2.SelectedRows[0].DataBoundItem;

            foreach (BE.CONCEPTO rec in r.ConceptosAplicados)
            {
                listBox1.Items.Add(rec);
            }
            
        }

        private void button3_Click(object sender, EventArgs e)
        {
            dataGridView2.DataSource = null;

            List<BE.RECIBO> recibos = BLL.RECIBO.Listar();

            BE.EMPLEADO empleado = (BE.EMPLEADO)dataGridView1.SelectedRows[0].DataBoundItem;

            List<BE.RECIBO> recibosMostrar = (from r in recibos
                                              where r.EMPLEADO.Legajo == empleado.Legajo && r.Fecha == dateTimePicker1.Text
                                              select r).ToList();

            dataGridView2.DataSource = recibosMostrar;

            listBox1.Items.Clear(); ;
        }

        private void FormRECIBOS_Enter(object sender, EventArgs e)
        {
            dataGridView1.DataSource = null;
            dataGridView1.DataSource = BLL.EMPLEADO.Listar();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            List<BE.RECIBO> recibos = BLL.RECIBO.Listar();

            dataGridView2.DataSource = null;
            dataGridView2.DataSource = recibos;
        }

        
    }
}

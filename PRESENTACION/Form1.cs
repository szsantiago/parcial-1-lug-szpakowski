﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PRESENTACION
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void eMPLEADOSToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FormEMPLEADOS fe = new FormEMPLEADOS();
            fe.Show();
            fe.MdiParent = this;
        }

        private void cONCEPTOSToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FormCONCEPTOS fc = new FormCONCEPTOS();
            fc.Show();
            fc.MdiParent = this;
        }

        private void rECIBOSDESUELDOToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FormRECIBOS fm = new FormRECIBOS();
            fm.Show();
            fm.MdiParent = this;
        }

        private void sALIRToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}

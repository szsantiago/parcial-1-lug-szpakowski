﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace PRESENTACION
{
    public partial class FormEMPLEADOS : Form
    {
        BLL.EMPLEADO empleado = new BLL.EMPLEADO();

        public FormEMPLEADOS()
        {
            InitializeComponent();
        }

        private void FormEMPLEADOS_Load(object sender, EventArgs e)
        {
            Listar();
        }

        private void Listar()
        {

            listBox1.DataSource = null;

            listBox1.DataSource = BLL.CONCEPTO.VerConceptos();
            

            dataGridView1.DataSource = null;
            dataGridView1.DataSource = BLL.EMPLEADO.Listar();
        }

        private bool Validar()
        {
            if (textBoxNombre.Text != "" && textBoxApellido.Text != "" && textBoxCUIL.Text != "" && dateTimePicker1.Value != null && textBoxSueldoBruto.Text != "")
            {
                if (textBoxCUIL.TextLength == 11)
                {
                    return true;
                }
                else
                {
                    MessageBox.Show("CUIL INCOMPLETO");
                    return false;
                }
                
            }
            else
            {
                return false;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {

            if(Validar())
            {
                BE.SUELDO sueldo = new BE.SUELDO();
                sueldo.SueldoBruto = float.Parse(textBoxSueldoBruto.Text);

                BLL.SUELDO s = new BLL.SUELDO();

                List<BE.CONCEPTO> conceptos = new List<BE.CONCEPTO>();
                

                foreach (BE.CONCEPTO item in listBox1.SelectedItems)
                {
                    conceptos.Add(item);
                }

                
                s.AltaSueldo(sueldo, conceptos);


                BE.EMPLEADO emp = new BE.EMPLEADO();
                emp.Nombre = textBoxNombre.Text;
                emp.Apellido = textBoxApellido.Text;
                emp.CUIL = textBoxCUIL.Text;
                emp.Alta = dateTimePicker1.Value;
                emp.SUELDO = sueldo;
                
                foreach (BE.CONCEPTO c in listBox1.SelectedItems)
                {
                    emp.Conceptos.Add(c);
                }
            
                empleado.Alta(emp);

                Listar();

                LImpiarTextBox();
            }


        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            
            BE.EMPLEADO emp = (BE.EMPLEADO)dataGridView1.SelectedRows[0].DataBoundItem;

            listBox2.DataSource = null;
            listBox2.DataSource = emp.Conceptos;

            textBoxNombre.Text = emp.Nombre;
            textBoxApellido.Text = emp.Apellido;
            textBoxCUIL.Text = emp.CUIL;
            dateTimePicker1.Value = emp.Alta;
            textBoxSueldoBruto.Text = emp.SUELDO.SueldoBruto.ToString();
            
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (Validar() && (BE.EMPLEADO)dataGridView1.SelectedRows[0].DataBoundItem != null)
            {

                BE.EMPLEADO emp = (BE.EMPLEADO)dataGridView1.SelectedRows[0].DataBoundItem;
                emp.Nombre = textBoxNombre.Text;
                emp.Apellido = textBoxApellido.Text;
                emp.CUIL = textBoxCUIL.Text;
                emp.Alta = dateTimePicker1.Value;
                emp.Conceptos.Clear();

                foreach (BE.CONCEPTO c in listBox1.SelectedItems)
                {
                    emp.Conceptos.Add(c);
                }

                BLL.SUELDO s = new BLL.SUELDO();
                emp.SUELDO.SueldoBruto = float.Parse(textBoxSueldoBruto.Text);
                s.ModificarSueldo(emp.SUELDO, emp.Conceptos);

                empleado.Modificar(emp);

                LImpiarTextBox();
                Listar();
            }            
        }

        private void button3_Click(object sender, EventArgs e)
        {
            BE.EMPLEADO emp = (BE.EMPLEADO)dataGridView1.SelectedRows[0].DataBoundItem;

            bool ok = empleado.Baja(emp);

            if (ok)
            {
                LImpiarTextBox();
                Listar();
            }
            else
            {
                MessageBox.Show("No se puede borrar el empleado porque tiene recibo de sueldo");
            }
            
        }

        private void textBoxCUIL_TextChanged(object sender, EventArgs e)
        {
            if (long.TryParse(textBoxCUIL.Text, out long R) == false)
            { textBoxCUIL.Text = ""; }
            
        }

        private void FormatoTexto(TextBox t)
        {
            if (t.Text.Contains("1") || t.Text.Contains("2") || t.Text.Contains("3") || t.Text.Contains("4") || t.Text.Contains("5") || t.Text.Contains("6") || t.Text.Contains("7") || t.Text.Contains("8") || t.Text.Contains("9") || t.Text.Contains("0") == true)
            {
                t.Text = "";
            }
        }

        private void textBoxNombre_TextChanged(object sender, EventArgs e)
        {
            FormatoTexto(textBoxNombre);
        }

        private void textBoxApellido_TextChanged(object sender, EventArgs e)
        {
            FormatoTexto(textBoxApellido);
        }

        private void textBoxSueldoBruto_TextChanged(object sender, EventArgs e)
        {
            if (float.TryParse(textBoxCUIL.Text, out float R) == false)
            { textBoxSueldoBruto.Text = ""; }
        }

        private void FormEMPLEADOS_Enter(object sender, EventArgs e)
        {
            Listar();
        }

        private void LImpiarTextBox()
        {
            textBoxNombre.Text = ""; textBoxApellido.Text = ""; textBoxCUIL.Text = ""; textBoxSueldoBruto.Text = ""; listBox2.DataSource = null;
        }

    }
}

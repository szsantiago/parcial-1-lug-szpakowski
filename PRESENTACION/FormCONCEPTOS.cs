﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PRESENTACION
{
    public partial class FormCONCEPTOS : Form
    {
        BLL.CONCEPTO concepto = new BLL.CONCEPTO();
        BE.CONCEPTO c = new BE.CONCEPTO();

        public FormCONCEPTOS()
        {
            InitializeComponent();
        }

        private void FormCONCEPTOS_Load(object sender, EventArgs e)
        {
            Actualizar();
        }

        private void Actualizar()
        {
            dataGridView1.DataSource = null;
            dataGridView1.DataSource = BLL.CONCEPTO.VerConceptos();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (float.TryParse(textBox2.Text,out float r) == true)
            {
                c.Descripcion = textBox1.Text;
                c.Porcentaje = float.Parse(textBox2.Text.Replace('.', ','));

                concepto.NuevoConcepto(c);
                Limpiar();
                Actualizar();
            }
            else
            {
                MessageBox.Show("Datos incorrectos");
            }
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            c = (BE.CONCEPTO)dataGridView1.SelectedRows[0].DataBoundItem;

            textBox1.Text = c.Descripcion;
            textBox2.Text = c.Porcentaje.ToString();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if ((BE.CONCEPTO)dataGridView1.SelectedRows[0].DataBoundItem != null && float.TryParse(textBox2.Text, out float r) == true)
            {
                c.Descripcion = textBox1.Text;
                c.Porcentaje = float.Parse(textBox2.Text.Replace('.', ','));

                concepto.ModificarConcepto(c);
                Limpiar();
                Actualizar();
            }          
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if ((BE.CONCEPTO)dataGridView1.SelectedRows[0].DataBoundItem != null)
            {
                concepto.BorrarConcepto(c);
                Limpiar();
                Actualizar();
            }
        }

        private void Limpiar()
        {
            textBox1.Text = "";
            textBox2.Text = "";
        }
        
    }
}

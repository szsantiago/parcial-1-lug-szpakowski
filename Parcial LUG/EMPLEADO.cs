﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BLL
{
    public class EMPLEADO
    {

        DAL.MP_EMPLEADO emp = new DAL.MP_EMPLEADO();

        public void Alta (BE.EMPLEADO e)
        {
            emp.Alta(e);
        }

        
        public void Modificar(BE.EMPLEADO e)
        {
            emp.Modificar(e);
        }

        public bool Baja (BE.EMPLEADO e)
        {
            bool ok = true;
            List<BE.RECIBO> recibos = DAL.MP_RECIBO.Listar();

            foreach (BE.RECIBO r in recibos)
            {
                if (r.EMPLEADO.Legajo == e.Legajo)
                {
                    ok = false;
                }
            }

            if (ok)
            {
                emp.Baja(e);
            }

            return ok;
        }

        public static List<BE.EMPLEADO> Listar()
        {
            return DAL.MP_EMPLEADO.Listar();
        }
    }
}
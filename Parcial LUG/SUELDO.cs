﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BLL
{
    public class SUELDO
    {

        DAL.MP_SUELDO sueldo = new DAL.MP_SUELDO();

        public void AltaSueldo(BE.SUELDO s, List<BE.CONCEPTO> conceptos)
        {
            s.TotalDescuentos = 0;

            foreach (BE.CONCEPTO conc in conceptos)
            {
                s.TotalDescuentos += s.SueldoBruto * (conc.Porcentaje) / 100;
            }

            s.SueldoNeto = s.SueldoBruto - float.Parse(s.TotalDescuentos.ToString("N2"));

            sueldo.AltaSueldo(s);
        }

        public void ModificarSueldo(BE.SUELDO s, List<BE.CONCEPTO> conceptos)
        {
            s.TotalDescuentos = 0;

            foreach (BE.CONCEPTO conc in conceptos)
            {
                s.TotalDescuentos += s.SueldoBruto * conc.Porcentaje / 100;
            }

            s.SueldoNeto = s.SueldoBruto - s.TotalDescuentos;

            sueldo.Modificar(s);
        }

    }
}
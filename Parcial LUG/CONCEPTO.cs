﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BLL
{
    public class CONCEPTO
    {

        DAL.MP_CONCEPTO conc = new DAL.MP_CONCEPTO();

        public void NuevoConcepto (BE.CONCEPTO c)
        {
            conc.Alta(c);
        }

        public void BorrarConcepto(BE.CONCEPTO c)
        {
            conc.Baja(c);
        }

        public void ModificarConcepto(BE.CONCEPTO c)
        {
            conc.Modificar(c);
        }

        public static List<BE.CONCEPTO> VerConceptos ()
        {
            return DAL.MP_CONCEPTO.Listar();
        }
    }
}
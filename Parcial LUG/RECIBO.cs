﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BLL
{
    public class RECIBO
    {
        DAL.MP_RECIBO recibo = new DAL.MP_RECIBO();

        public void Alta(BE.EMPLEADO e, string fecha)
        {
            BE.RECIBO r = new BE.RECIBO();

            r.EMPLEADO = e;
            r.Fecha = fecha;
            r.Subtotal = e.SUELDO.SueldoBruto;
            r.Total = e.SUELDO.SueldoNeto;
            r.ConceptosAplicados = e.Conceptos;
            r.Descuentos = e.SUELDO.TotalDescuentos;

            recibo.Alta(r);
        }

        public static List<BE.RECIBO> Listar()
        {
            return DAL.MP_RECIBO.Listar();
        }
            

    }
}
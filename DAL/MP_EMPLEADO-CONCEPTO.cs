﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;

namespace DAL
{
    internal class MP_EMPLEADO_CONCEPTO
    {
        static ACCESO acceso = new ACCESO();
        static List<SqlParameter> parametros = new List<SqlParameter>();

        static public void Alta(BE.EMPLEADO e)
        {
            acceso.Abrir();
            int leg = acceso.LeerEscalar("UltimoID", acceso.CrearParametro("@NombreTabla", "EMPLEADO"));
            acceso.Cerrar();

            foreach (BE.CONCEPTO c in e.Conceptos)
            {
                parametros.Clear();

                parametros.Add(acceso.CrearParametro("@idEmpleado", leg));
                parametros.Add(acceso.CrearParametro("@idConcepto", c.ID));

                acceso.Abrir();
                acceso.Escribir("AltaEmpleado_Concepto", parametros);
                acceso.Cerrar();
            }
        }

        static public void Modificacion(BE.EMPLEADO e)
        {
            parametros.Clear();
            acceso.Abrir();
            parametros.Add(acceso.CrearParametro("@Legajo", e.Legajo));
            acceso.Escribir("BorrarEmpleado_Concepto", parametros);
            acceso.Cerrar();


            foreach (BE.CONCEPTO c in e.Conceptos)
            {
                parametros.Clear();

                parametros.Add(acceso.CrearParametro("@idEmpleado", e.Legajo));
                parametros.Add(acceso.CrearParametro("@idConcepto", c.ID));

                acceso.Abrir();
                acceso.Escribir("AltaEmpleado_Concepto", parametros);
                acceso.Cerrar();
            }
        }

        static public void Baja(BE.EMPLEADO e)
        {
            parametros.Clear();

            acceso.Abrir();
            parametros.Add(acceso.CrearParametro("@Legajo", e.Legajo));
            acceso.Escribir("BorrarEmpleado_Concepto", parametros);
            acceso.Cerrar();

        }

        static internal List<BE.CONCEPTO> ListarConceptosEmpleado(BE.EMPLEADO e)
        {
            List<BE.CONCEPTO> conceptos = MP_CONCEPTO.Listar();
            List<BE.CONCEPTO> conceptosEmpleado = new List<BE.CONCEPTO>();

            parametros.Clear();

            acceso.Abrir();
            parametros.Add(acceso.CrearParametro("@Legajo", e.Legajo));
            DataTable tablaconCeptosEmpleado = acceso.Leer("ListaConceptosEmpleado", parametros);
            acceso.Cerrar();


            foreach (DataRow registro in tablaconCeptosEmpleado.Rows)           //POR CADA CONCEPTO DE EMPLEADO
            {
                if (e.Legajo == int.Parse(registro[0].ToString()))              //SI EL LEGAJO = ID EMPLEADO
                {
                    foreach (BE.CONCEPTO c in conceptos)                        //POR CADA CONCEPTO
                    {
                        if (c.ID == int.Parse(registro[1].ToString()))          //SI ID DEL CONCEPTO = ID CONCEPTO EMPLEADO
                        {
                            conceptosEmpleado.Add(c);                           //AGREGAR CONCEPTO
                        }
                    }
                }

            }

            return conceptosEmpleado;                                           //DEVUELVE CONCEPTOS DEL EMPLEADO
        }
    }
}

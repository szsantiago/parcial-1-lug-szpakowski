﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;

namespace DAL
{
    public class MP_SUELDO
    {

        private ACCESO acceso = new ACCESO();

        public void AltaSueldo(BE.SUELDO s)
        {

            List<SqlParameter> parametros = new List<SqlParameter>();

            parametros.Add(acceso.CrearParametro("@SueldoBruto", s.SueldoBruto));
            parametros.Add(acceso.CrearParametro("@Descuentos", s.TotalDescuentos));
            parametros.Add(acceso.CrearParametro("@SueldoNeto", s.SueldoNeto));

            acceso.Abrir();
            acceso.Escribir("AltaSueldo", parametros);

            
            s.ID = acceso.LeerEscalar("UltimoID", acceso.CrearParametro("@NombreTabla", "Sueldo"));


            acceso.Cerrar();
        }

        public void Modificar(BE.SUELDO s)
        {
            List<SqlParameter> parametros = new List<SqlParameter>();

            parametros.Add(acceso.CrearParametro("@idSueldo", s.ID));
            parametros.Add(acceso.CrearParametro("@SueldoBruto", s.SueldoBruto));
            parametros.Add(acceso.CrearParametro("@Descuento", s.TotalDescuentos));
            parametros.Add(acceso.CrearParametro("@SueldoNeto", s.SueldoNeto));

            acceso.Abrir();
            acceso.Escribir("ModificarSueldo", parametros);

            acceso.Cerrar();
        }

        static internal void Baja(BE.EMPLEADO e)
        {
            ACCESO acceso = new ACCESO();
            List<SqlParameter> parametros = new List<SqlParameter>();

            acceso.Abrir();
            parametros.Add(acceso.CrearParametro("@idSueldo", e.SUELDO.ID));

            acceso.Escribir("BajaSueldo", parametros);
            acceso.Cerrar();
        }

        static private List<BE.SUELDO> ListarSueldos()
        {
            ACCESO acceso = new ACCESO();

            List<BE.SUELDO> sueldos = new List<BE.SUELDO>();

            acceso.Abrir();
            DataTable tabla = acceso.Leer("ListarSueldos");
            acceso.Cerrar();

            foreach (DataRow reg in tabla.Rows)
            {
                BE.SUELDO s = new BE.SUELDO();
                s.ID = int.Parse(reg[0].ToString());
                s.SueldoBruto = float.Parse(reg[1].ToString());
                s.SueldoNeto = float.Parse(reg[2].ToString());
                s.TotalDescuentos = float.Parse(reg[3].ToString());

                sueldos.Add(s);
            }
            return sueldos;
        }

        static internal BE.SUELDO ListarSueldoEmpleado(int idSueldo)
        {
            List<BE.SUELDO> sueldos = ListarSueldos();

            foreach (BE.SUELDO suel in sueldos)
            {
                if (suel.ID == idSueldo)                        //SI ID DE SUELDO = ID DEL SUELDO DEL EMPLEADO
                {
                    return suel;
                }
            }

            return null;
           
        }
    }
}

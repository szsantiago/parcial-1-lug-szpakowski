﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;

namespace DAL
{
    public class MP_EMPLEADO
    {
        ACCESO acceso = new ACCESO();

        public void Alta(BE.EMPLEADO e)
        {
            acceso.Abrir();

            List<SqlParameter> parametros = new List<SqlParameter>();

            parametros.Add(acceso.CrearParametro("@Nombre", e.Nombre));
            parametros.Add(acceso.CrearParametro("@Apellido", e.Apellido));
            parametros.Add(acceso.CrearParametro("@cuil", e.CUIL));
            parametros.Add(acceso.CrearParametro("@alta", e.Alta));
            parametros.Add(acceso.CrearParametro("@idSueldo", e.SUELDO.ID));

            acceso.Escribir("AltaEmpleado", parametros);                                    //ALTA EMPLEADO A TABLA
            acceso.Cerrar();

            MP_EMPLEADO_CONCEPTO.Alta(e);
        }

        public void Modificar(BE.EMPLEADO e)
        {
            acceso.Abrir();

            List<SqlParameter> parametros = new List<SqlParameter>();

            parametros.Add(acceso.CrearParametro("@id", e.Legajo));
            parametros.Add(acceso.CrearParametro("@Nombre", e.Nombre));
            parametros.Add(acceso.CrearParametro("@Apellido", e.Apellido));
            parametros.Add(acceso.CrearParametro("@cuil", e.CUIL));
            parametros.Add(acceso.CrearParametro("@alta", e.Alta));
            parametros.Add(acceso.CrearParametro("@idSueldo", e.SUELDO.ID));

            acceso.Escribir("ModificarEmpleado", parametros);                   //MODIFICAR EMPLEADO
            acceso.Cerrar();

            parametros.Clear();

            MP_EMPLEADO_CONCEPTO.Modificacion(e);       //MODIFICAR TABLA EMPLEADO_CONCEPTO

            
        }

        public void Baja (BE.EMPLEADO e)
        {
            List<SqlParameter> parametros = new List<SqlParameter>();

            MP_EMPLEADO_CONCEPTO.Baja(e);                               //BORRAR EMPLEADO_COCEPTO

            acceso.Abrir();
            parametros.Add(acceso.CrearParametro("@id", e.Legajo));

            acceso.Escribir("BajaEmpleado", parametros);                //BORRAR EMPLEADO
            acceso.Cerrar();

            parametros.Clear();

            MP_SUELDO.Baja(e);                                          //BORRAR SUELDO DEL EMPLEADO
            
        }

        static public List<BE.EMPLEADO> Listar()
        {
            List<BE.EMPLEADO> empleados = new List<BE.EMPLEADO>();

            ACCESO acceso1 = new ACCESO();
            acceso1.Abrir();
            DataTable tabla = acceso1.Leer("ListarEmpleados");
            acceso1.Cerrar();

            List<SqlParameter> parametros = new List<SqlParameter>();

            foreach (DataRow reg in tabla.Rows)                                 //POR CADA EMPLEADO
            {
                BE.EMPLEADO e = new BE.EMPLEADO();

                e.Legajo = int.Parse(reg[0].ToString());
                e.Nombre = reg[1].ToString();
                e.Apellido = reg[2].ToString();
                e.CUIL = reg[3].ToString();
                e.Alta = DateTime.Parse(reg[4].ToString());

                parametros.Clear();

                e.Conceptos = MP_EMPLEADO_CONCEPTO.ListarConceptosEmpleado(e);

                e.SUELDO = MP_SUELDO.ListarSueldoEmpleado(int.Parse(reg[5].ToString()));

                empleados.Add(e);
            }

            return empleados;
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;

namespace DAL
{
    public class MP_RECIBO
    {
        ACCESO acceso = new ACCESO();

        public void Alta(BE.RECIBO recibo)
        {
            acceso.Abrir();

            List<SqlParameter> parametros = new List<SqlParameter>();

            parametros.Add(acceso.CrearParametro("@fecha", recibo.Fecha));
            parametros.Add(acceso.CrearParametro("@id_Empleado", recibo.EMPLEADO.Legajo));
            parametros.Add(acceso.CrearParametro("@subtotal", recibo.Subtotal));
            parametros.Add(acceso.CrearParametro("@total", recibo.Total));
            parametros.Add(acceso.CrearParametro("@descuentos", recibo.Descuentos));

            acceso.Escribir("AltaRecibo", parametros);
            acceso.Cerrar();

            MP_RECIBO_CONCEPTO.Alta(recibo.ConceptosAplicados);

        }

        static public List<BE.RECIBO> Listar()
        {
            ACCESO acceso = new ACCESO();

            List<BE.RECIBO> recibos = new List<BE.RECIBO>();
            List<BE.CONCEPTO> conceptos = new List<BE.CONCEPTO>();

            acceso.Abrir();
            DataTable tabla = acceso.Leer("ListarRecibos");
            DataTable tabla2 = acceso.Leer("ListarRecibo_Concepto");
            DataTable tabla3 = acceso.Leer("ListarConceptos");
            acceso.Cerrar();

            foreach (DataRow conc in tabla3.Rows)
            {
                BE.CONCEPTO c = new BE.CONCEPTO();
                c.ID = int.Parse(conc[0].ToString());
                c.Descripcion = conc[1].ToString();
                c.Porcentaje = float.Parse(conc[2].ToString());

                conceptos.Add(c);
            }


            List<BE.EMPLEADO> empleados = MP_EMPLEADO.Listar();

            foreach (DataRow reg in tabla.Rows)
            {
                BE.RECIBO r = new BE.RECIBO();
                r.Id = int.Parse(reg[0].ToString());
                r.Fecha = reg[1].ToString();
                r.Subtotal = float.Parse(reg[3].ToString());
                r.Total = float.Parse(reg[4].ToString());
                r.Descuentos = float.Parse(reg[5].ToString());

                foreach (BE.EMPLEADO e in empleados)
                {
                    if (int.Parse(reg[2].ToString()) == e.Legajo)
                    {
                        r.EMPLEADO = e;                                 
                    }
                }

                foreach (DataRow row in tabla2.Rows)
                {
                    if (int.Parse(row[0].ToString()) == r.Id)
                    {
                        foreach (BE.CONCEPTO conc in conceptos)
                        {
                            if (conc.ID == int.Parse(row[1].ToString()))
                            {
                                r.ConceptosAplicados.Add(conc);                         //CONCEPTOS APLICADOS AL RECIBO
                            }
                        }
                    }
                }

                recibos.Add(r);
            }

            return recibos;
        }

    }
}

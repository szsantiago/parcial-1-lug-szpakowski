﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;

namespace DAL
{
    public class MP_CONCEPTO
    {
        ACCESO acceso = new ACCESO();

        public void Alta (BE.CONCEPTO c)
        {
            acceso.Abrir();

            List<SqlParameter> parametros = new List<SqlParameter>();

            parametros.Add(acceso.CrearParametro("@Nombre", c.Descripcion));
            parametros.Add(acceso.CrearParametro("@Porcentaje", c.Porcentaje));

            acceso.Escribir("AltaConcepto", parametros);
            acceso.Cerrar();
        }

        public void Modificar (BE.CONCEPTO c)
        {
            acceso.Abrir();

            List<SqlParameter> parametros = new List<SqlParameter>();

            parametros.Add(acceso.CrearParametro("@id", c.ID));
            parametros.Add(acceso.CrearParametro("@Descripcion", c.Descripcion));
            parametros.Add(acceso.CrearParametro("@Porcentaje", c.Porcentaje));

            acceso.Escribir("ModificarConcepto", parametros);
            acceso.Cerrar();
        }

        public void Baja (BE.CONCEPTO c)
        {
            acceso.Abrir();

            List<SqlParameter> parametros = new List<SqlParameter>();
            parametros.Add(acceso.CrearParametro("id", c.ID));

            acceso.Escribir("BajaConcepto", parametros);
            acceso.Cerrar();
        }

        public static List<BE.CONCEPTO> Listar()
        {
            List<BE.CONCEPTO> conceptos = new List<BE.CONCEPTO>();
 
            ACCESO acceso2 = new ACCESO();

            acceso2.Abrir();
            DataTable tabla = acceso2.Leer("ListarConceptos");
            acceso2.Cerrar();

            foreach (DataRow reg in tabla.Rows)
            {
                BE.CONCEPTO c = new BE.CONCEPTO();

                c.ID = int.Parse(reg[0].ToString());
                c.Descripcion = reg[1].ToString();
                c.Porcentaje = float.Parse(reg[2].ToString());

                conceptos.Add(c);
            }
            return conceptos;
        }

        

    }
}

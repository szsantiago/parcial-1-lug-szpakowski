﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;

namespace DAL
{
    internal class MP_RECIBO_CONCEPTO
    {
        static ACCESO acceso = new ACCESO();

        internal static void Alta (List<BE.CONCEPTO> conceptosaplicados)
        {
            List<SqlParameter> parametros = new List<SqlParameter>();

            foreach (BE.CONCEPTO conc in conceptosaplicados)
            {
                acceso.Abrir();
                parametros.Clear();

                parametros.Add(acceso.CrearParametro("@idRecibo", acceso.LeerEscalar("UltimoID", acceso.CrearParametro("@NombreTabla", "RECIBO"))));
                parametros.Add(acceso.CrearParametro("@idConcepto", conc.ID));


                acceso.Escribir("AltaRecibo_Concepto", parametros);                 //COMPLETAR TABLA RECIBO_CONCEPTO
                acceso.Cerrar();
            }
        }
        
    }
}

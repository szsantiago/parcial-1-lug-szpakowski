USE [master]
GO
/****** Object:  Database [SUELDOS_EMPLEADOS]    Script Date: 27/9/2020 17:06:23 ******/
CREATE DATABASE [SUELDOS_EMPLEADOS]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'SUELDOS_EMPLEADOS', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.SQLEXPRESS\MSSQL\DATA\SUELDOS_EMPLEADOS.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'SUELDOS_EMPLEADOS_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.SQLEXPRESS\MSSQL\DATA\SUELDOS_EMPLEADOS_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
 WITH CATALOG_COLLATION = DATABASE_DEFAULT
GO
ALTER DATABASE [SUELDOS_EMPLEADOS] SET COMPATIBILITY_LEVEL = 150
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [SUELDOS_EMPLEADOS].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [SUELDOS_EMPLEADOS] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [SUELDOS_EMPLEADOS] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [SUELDOS_EMPLEADOS] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [SUELDOS_EMPLEADOS] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [SUELDOS_EMPLEADOS] SET ARITHABORT OFF 
GO
ALTER DATABASE [SUELDOS_EMPLEADOS] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [SUELDOS_EMPLEADOS] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [SUELDOS_EMPLEADOS] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [SUELDOS_EMPLEADOS] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [SUELDOS_EMPLEADOS] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [SUELDOS_EMPLEADOS] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [SUELDOS_EMPLEADOS] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [SUELDOS_EMPLEADOS] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [SUELDOS_EMPLEADOS] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [SUELDOS_EMPLEADOS] SET  DISABLE_BROKER 
GO
ALTER DATABASE [SUELDOS_EMPLEADOS] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [SUELDOS_EMPLEADOS] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [SUELDOS_EMPLEADOS] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [SUELDOS_EMPLEADOS] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [SUELDOS_EMPLEADOS] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [SUELDOS_EMPLEADOS] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [SUELDOS_EMPLEADOS] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [SUELDOS_EMPLEADOS] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [SUELDOS_EMPLEADOS] SET  MULTI_USER 
GO
ALTER DATABASE [SUELDOS_EMPLEADOS] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [SUELDOS_EMPLEADOS] SET DB_CHAINING OFF 
GO
ALTER DATABASE [SUELDOS_EMPLEADOS] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [SUELDOS_EMPLEADOS] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [SUELDOS_EMPLEADOS] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [SUELDOS_EMPLEADOS] SET QUERY_STORE = OFF
GO
USE [SUELDOS_EMPLEADOS]
GO
/****** Object:  Table [dbo].[CONCEPTO]    Script Date: 27/9/2020 17:06:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CONCEPTO](
	[ID] [int] NOT NULL,
	[DESCRIPCION] [varchar](50) NOT NULL,
	[PORCENTAJE] [float] NOT NULL,
 CONSTRAINT [PK_CONCEPTO] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[EMPLEADO]    Script Date: 27/9/2020 17:06:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EMPLEADO](
	[LEGAJO] [int] NOT NULL,
	[NOMBRE] [varchar](50) NOT NULL,
	[APELLIDO] [varchar](50) NOT NULL,
	[CUIL] [ntext] NOT NULL,
	[ALTA] [date] NOT NULL,
	[SUELDO] [int] NULL,
 CONSTRAINT [PK_EMPLEADO] PRIMARY KEY CLUSTERED 
(
	[LEGAJO] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[EMPLEADO_CONCEPTO]    Script Date: 27/9/2020 17:06:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EMPLEADO_CONCEPTO](
	[ID_EMPLEADO] [int] NOT NULL,
	[ID_CONCEPTO] [int] NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[RECIBO]    Script Date: 27/9/2020 17:06:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RECIBO](
	[ID] [int] NOT NULL,
	[FECHA] [varchar](50) NOT NULL,
	[ID_EMPLEADO] [int] NOT NULL,
	[SUBTOTAL] [float] NOT NULL,
	[TOTAL] [float] NOT NULL,
	[DESCUENTOS] [float] NOT NULL,
 CONSTRAINT [PK_RECIBO] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[RECIBO_CONCEPTO]    Script Date: 27/9/2020 17:06:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RECIBO_CONCEPTO](
	[ID_RECIBO] [int] NOT NULL,
	[ID_CONCEPTO] [int] NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[SUELDO]    Script Date: 27/9/2020 17:06:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SUELDO](
	[ID] [int] NOT NULL,
	[SUELDO_BRUTO] [float] NOT NULL,
	[SUELDO_NETO] [float] NULL,
	[DESCUENTOS] [float] NULL,
 CONSTRAINT [PK_SUELDO] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[EMPLEADO]  WITH CHECK ADD  CONSTRAINT [FK_EMPLEADO_SUELDO] FOREIGN KEY([SUELDO])
REFERENCES [dbo].[SUELDO] ([ID])
GO
ALTER TABLE [dbo].[EMPLEADO] CHECK CONSTRAINT [FK_EMPLEADO_SUELDO]
GO
ALTER TABLE [dbo].[EMPLEADO_CONCEPTO]  WITH CHECK ADD  CONSTRAINT [FK_EMPLEADO_CONCEPTO_CONCEPTO] FOREIGN KEY([ID_CONCEPTO])
REFERENCES [dbo].[CONCEPTO] ([ID])
GO
ALTER TABLE [dbo].[EMPLEADO_CONCEPTO] CHECK CONSTRAINT [FK_EMPLEADO_CONCEPTO_CONCEPTO]
GO
ALTER TABLE [dbo].[EMPLEADO_CONCEPTO]  WITH CHECK ADD  CONSTRAINT [FK_EMPLEADO_CONCEPTO_EMPLEADO] FOREIGN KEY([ID_EMPLEADO])
REFERENCES [dbo].[EMPLEADO] ([LEGAJO])
GO
ALTER TABLE [dbo].[EMPLEADO_CONCEPTO] CHECK CONSTRAINT [FK_EMPLEADO_CONCEPTO_EMPLEADO]
GO
ALTER TABLE [dbo].[RECIBO]  WITH CHECK ADD  CONSTRAINT [FK_RECIBO_EMPLEADO] FOREIGN KEY([ID_EMPLEADO])
REFERENCES [dbo].[EMPLEADO] ([LEGAJO])
GO
ALTER TABLE [dbo].[RECIBO] CHECK CONSTRAINT [FK_RECIBO_EMPLEADO]
GO
ALTER TABLE [dbo].[RECIBO_CONCEPTO]  WITH CHECK ADD  CONSTRAINT [FK_RECIBO_CONCEPTO_CONCEPTO] FOREIGN KEY([ID_CONCEPTO])
REFERENCES [dbo].[CONCEPTO] ([ID])
GO
ALTER TABLE [dbo].[RECIBO_CONCEPTO] CHECK CONSTRAINT [FK_RECIBO_CONCEPTO_CONCEPTO]
GO
ALTER TABLE [dbo].[RECIBO_CONCEPTO]  WITH CHECK ADD  CONSTRAINT [FK_RECIBO_CONCEPTO_RECIBO] FOREIGN KEY([ID_RECIBO])
REFERENCES [dbo].[RECIBO] ([ID])
GO
ALTER TABLE [dbo].[RECIBO_CONCEPTO] CHECK CONSTRAINT [FK_RECIBO_CONCEPTO_RECIBO]
GO
/****** Object:  StoredProcedure [dbo].[AltaConcepto]    Script Date: 27/9/2020 17:06:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[AltaConcepto]

@Nombre varchar(50), @Porcentaje float

as

	declare @id int 
	set @id = ISNULL((select MAX(ID) from CONCEPTO), 0)+ 1

	insert into CONCEPTO (ID, DESCRIPCION, PORCENTAJE)
	values (@id, @Nombre, @Porcentaje)

GO
/****** Object:  StoredProcedure [dbo].[AltaEmpleado]    Script Date: 27/9/2020 17:06:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[AltaEmpleado]

@Nombre VARCHAR(50), @Apellido varchar(50), @cuil ntext, @alta date, @idSueldo int

as

	declare @id int 
	set @id = ISNULL((select MAX(LEGAJO) from EMPLEADO), 0)+ 1

	insert into EMPLEADO (LEGAJO, NOMBRE, APELLIDO, CUIL, ALTA, SUELDO)
	values (@id, @Nombre, @Apellido, @cuil, @alta, @idSueldo)

GO
/****** Object:  StoredProcedure [dbo].[AltaEmpleado_Concepto]    Script Date: 27/9/2020 17:06:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[AltaEmpleado_Concepto]

@idEmpleado int, @idConcepto int

as
	insert into EMPLEADO_CONCEPTO (ID_EMPLEADO, ID_CONCEPTO)
	values (@idEmpleado, @idConcepto)
GO
/****** Object:  StoredProcedure [dbo].[AltaRecibo]    Script Date: 27/9/2020 17:06:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[AltaRecibo]

@fecha varchar(50), @id_Empleado int, @subtotal float, @total float, @descuentos float

as

	declare @id int 
	set @id = ISNULL((select MAX(ID) from RECIBO), 0)+ 1

	insert into RECIBO (ID, FECHA, ID_EMPLEADO, SUBTOTAL,TOTAL, DESCUENTOS)
	values (@id, @fecha, @id_Empleado, @subtotal, @total, @descuentos);

GO
/****** Object:  StoredProcedure [dbo].[AltaRecibo_Concepto]    Script Date: 27/9/2020 17:06:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[AltaRecibo_Concepto]

@idRecibo int, @idConcepto int

as

	insert into RECIBO_CONCEPTO (ID_RECIBO, ID_CONCEPTO)
	values (@idRecibo, @idConcepto)

GO
/****** Object:  StoredProcedure [dbo].[AltaSueldo]    Script Date: 27/9/2020 17:06:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[AltaSueldo]

@SueldoBruto float, @Descuentos float, @SueldoNeto float

as

	declare @id int 
	set @id = isnull((select MAX(ID) from SUELDO), 0) + 1
	
	insert into SUELDO (ID, SUELDO_BRUTO, SUELDO_NETO, DESCUENTOS)
	values (@id, @SueldoBruto, @SueldoNeto, @Descuentos)
GO
/****** Object:  StoredProcedure [dbo].[BajaConcepto]    Script Date: 27/9/2020 17:06:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[BajaConcepto]

@id int

as

	delete CONCEPTO 
	where ID = @id

GO
/****** Object:  StoredProcedure [dbo].[BajaEmpleado]    Script Date: 27/9/2020 17:06:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[BajaEmpleado]

@id int

as

	delete EMPLEADO 
	where LEGAJO = @id

GO
/****** Object:  StoredProcedure [dbo].[BajaSueldo]    Script Date: 27/9/2020 17:06:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[BajaSueldo]

@idSueldo int

as
	delete from SUELDO
	where ID = @idSueldo
GO
/****** Object:  StoredProcedure [dbo].[BorrarEmpleado_Concepto]    Script Date: 27/9/2020 17:06:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[BorrarEmpleado_Concepto]

@Legajo int

as

	delete from EMPLEADO_CONCEPTO
	where ID_EMPLEADO = @Legajo

GO
/****** Object:  StoredProcedure [dbo].[ListaConceptosEmpleado]    Script Date: 27/9/2020 17:06:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[ListaConceptosEmpleado]

@Legajo int

as

	select * from EMPLEADO_CONCEPTO
	where ID_EMPLEADO = @Legajo
	

GO
/****** Object:  StoredProcedure [dbo].[ListarConceptos]    Script Date: 27/9/2020 17:06:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[ListarConceptos]

as

	select * from CONCEPTO

GO
/****** Object:  StoredProcedure [dbo].[ListarEmpleados]    Script Date: 27/9/2020 17:06:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[ListarEmpleados]

as

	select * from EMPLEADO

GO
/****** Object:  StoredProcedure [dbo].[ListarRecibo_Concepto]    Script Date: 27/9/2020 17:06:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[ListarRecibo_Concepto]

as
	select * from RECIBO_CONCEPTO
GO
/****** Object:  StoredProcedure [dbo].[ListarRecibos]    Script Date: 27/9/2020 17:06:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[ListarRecibos]

as

	select * from RECIBO

GO
/****** Object:  StoredProcedure [dbo].[ListarSueldos]    Script Date: 27/9/2020 17:06:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[ListarSueldos]

as
	select * from SUELDO
GO
/****** Object:  StoredProcedure [dbo].[ModificarConcepto]    Script Date: 27/9/2020 17:06:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[ModificarConcepto]

@id int, @Descripcion varchar(50), @Porcentaje float

as

	update CONCEPTO 
	set DESCRIPCION = @Descripcion, PORCENTAJE = @Porcentaje
	where ID = @id

GO
/****** Object:  StoredProcedure [dbo].[ModificarEmpleado]    Script Date: 27/9/2020 17:06:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[ModificarEmpleado]

@id int, @Nombre VARCHAR(50), @Apellido varchar(50), @cuil ntext, @alta date, @idSueldo int

as

	update EMPLEADO 
	set NOMBRE = @Nombre, APELLIDO = @Apellido, CUIL = @cuil, ALTA = @alta, SUELDO = @idSueldo
	where LEGAJO = @id

GO
/****** Object:  StoredProcedure [dbo].[ModificarSueldo]    Script Date: 27/9/2020 17:06:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[ModificarSueldo]

@idSueldo int, @SueldoBruto float, @Descuento float, @SueldoNeto float

as
	update SUELDO
	set SUELDO_BRUTO = @SueldoBruto, SUELDO_NETO = @SueldoNeto, DESCUENTOS = @Descuento
	where ID = @idSueldo
GO
/****** Object:  StoredProcedure [dbo].[UltimoID]    Script Date: 27/9/2020 17:06:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[UltimoID]

@NombreTabla varchar(50) 

as
	SET @NombreTabla = UPPER(@NombreTabla)

	if(@NombreTabla = 'EMPLEADO')
	BEGIN
		SELECT isnull((select MAX(LEGAJO) from EMPLEADO), 1)
	END
	else if (@NombreTabla = 'SUELDO')
	BEGIN
		SELECT isnull((select MAX(ID) from SUELDO), 1)
	END
	else if (@NombreTabla = 'RECIBO')
	BEGIN
		SELECT ISNULL((select MAX(ID) from RECIBO), 1)
	END
	
GO
USE [master]
GO
ALTER DATABASE [SUELDOS_EMPLEADOS] SET  READ_WRITE 
GO
